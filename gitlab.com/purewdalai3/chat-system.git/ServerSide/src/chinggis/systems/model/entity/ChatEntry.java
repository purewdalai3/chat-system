package chinggis.systems.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import chinggis.systems.model.entity.Chat;

import javax.persistence.ManyToOne;

@Entity
@Table(name = "ChatEntry")
public class ChatEntry implements Serializable {

	private static final long serialVersionUID = 1L;

	public ChatEntry() {
	}

	@Id
	@GeneratedValue
	private long id;
	private String content;
	private String timestamp;
	@ManyToOne
	private Chat chat;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String param) {
		this.content = param;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String param) {
		this.timestamp = param;
	}

	public Chat getChat() {
	    return chat;
	}

	public void setChat(Chat param) {
	    this.chat = param;
	}

}