package chinggis.systems.model.service;

import java.util.List;

import chinggis.systems.model.entity.ChatEntry;

public interface ChatEntryService {
	/**
	 * Хэрэглэгчийн чат хадгалах функц
	 * 
	 * @param chatEntry
	 *            чатын агуулга , огноо , хэн бичсэн
	 */
	void save(ChatEntry chatEntry);

	/**
	 * Классыг ажилуулахад бэлэн болгоно.
	 */
	void init();

	/**
	 * Сүүлд бичсэн 10 чат
	 * 
	 * @return List<ChatEntry>
	 */
	List<ChatEntry> getLastTenChat();
	/**
	 * DB-с салгах
	 * */
	void close();
}
