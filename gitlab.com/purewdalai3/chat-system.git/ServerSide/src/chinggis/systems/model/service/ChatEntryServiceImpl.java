package chinggis.systems.model.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import chinggis.systems.model.entity.ChatEntry;

public class ChatEntryServiceImpl implements ChatEntryService {

	EntityManagerFactory ef;
	EntityManager em;
	
	public ChatEntryServiceImpl() {
		init();
	}
	
	@Override
	public void init() {
		ef = Persistence.createEntityManagerFactory("ServerSide");
		em = ef.createEntityManager();
	}
	
	@Override
	public void save(ChatEntry chatEntry) {
		em.persist(chatEntry);
		em.getTransaction().begin();
		em.getTransaction().commit();
	}

	@Override
	public void close() {
		em.close();
		ef.close();
	}

	@Override
	public List<ChatEntry> getLastTenChat() {
		Query query = em.createQuery("SELECT c FROM ChatEntry c ORDER BY c.timestamp DESC");
		query.setMaxResults(10);
		List<ChatEntry> result = query.getResultList();
		if(!result.isEmpty()) return result;
		return null;
	}
}
