package chinggis.systems.model.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import chinggis.systems.model.entity.Chat;
import chinggis.systems.model.entity.User;

public class UserServiceImpl implements UserService {
	EntityManagerFactory ef;
	EntityManager em;

	public UserServiceImpl() {
		ef = Persistence.createEntityManagerFactory("ServerSide");
		em = ef.createEntityManager();
	}

	@Override
	public void save(User user) {
		Chat chat = new Chat();
		em.persist(chat);
		user.setChat(chat);
		em.getTransaction().begin();
		em.persist(user);
		em.getTransaction().commit();
		close();
	}

	@Override
	public User findByNameAndPassword(String name, String password) {
		Query query = em.createQuery("Select u From User u WHERE u.name = :name AND u.password = :password");
		query.setParameter("name", name);
		query.setParameter("password", password);
		List<User> result = query.getResultList();
		User user = null;
		if (!result.isEmpty()) {
			user = (User) query.getSingleResult();
		}
		return user;
	}
	
	@Override
	public void close() {
		em.close();
		ef.close();
	}

	@Override
	public User findByName(String name) {
		Query query = em.createQuery("Select u From User u WHERE u.name = :name");
		query.setParameter("name", name);
		List<User> result = query.getResultList();
		User user = null;
		if (!result.isEmpty()) {
			user = (User) query.getSingleResult();
		}
		return user;
	}

	@Override
	public User findByChatId(String chatId) {
		Query query = em.createQuery("Select u From User u WHERE u.chat.id = :chat_id");
		query.setParameter("chat_id", Integer.parseInt(chatId));
		List<User> result = query.getResultList();
		User user = null;
		if (!result.isEmpty()) {
			user = (User) query.getSingleResult();
		}
		return user;
	}
}
