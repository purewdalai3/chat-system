package chinggis.systems.service;

import java.util.HashSet;

public interface MainService {

	/**
	 * Серверийг асаана. Хэрэглэгч илгээсэн хүсэлтэнд тохирох хариу өгнө.
	 */
	void run();

	/**
	 * Серверийн тохиргоо хийнэ.
	 * 
	 * @param Серверийн
	 *            порт дугаар
	 */
	void connect(int port);

	/**
	 * Серверээс хэрэглэгч рүү хариу илгээх
	 * 
	 * @param type
	 *            төрөл
	 * @param result
	 *            хариу , үр дүн
	 */
	void sendResult(int type, String result);

	/**
	 * Серверээс бүх хэрэглэгч рүү хариу илгээх
	 * 
	 * @param type
	 *            төрөл
	 * @param result
	 *            хариу , үр дүн
	 */
	void sendGlobalResult(int type, String result);

	/**
	 * Серверээр хэрэглэгч нэвтрэх үед тохирох хариу илгээх
	 * @param type
	 *            төрөл
	 * @param result
	 *            хариу , үр дүн
	 */
	void sendLoginResult(int type, String data);
	/**
	 * 
	 * */
	void init(String new_name);

}
