package chinggis.systems.controller;

import chinggis.systems.service.ClientSocketImpl;
import chinggis.systems.service.MainService;
import chinggis.systems.view.ChatView;
import chinggis.systems.view.ChatViewImpl;
import chinggis.systems.view.LoginView;
import chinggis.systems.view.LoginViewImpl;
import chinggis.systems.view.RegisterView;
import chinggis.systems.view.RegisterViewImpl;

public class ClientMain {

	public static void main(String[] args) {

		final int port = 1111;
		MainService service = new ClientSocketImpl();
		service.init("localhost", port);

		ClientController controller = new ClientController();
		LoginView loginView = new LoginViewImpl(controller);
		RegisterView registerView = new RegisterViewImpl(controller);
		ChatView chatView = new ChatViewImpl(controller);

		controller.init(service, loginView, registerView, chatView);
		controller.run();
	}
}