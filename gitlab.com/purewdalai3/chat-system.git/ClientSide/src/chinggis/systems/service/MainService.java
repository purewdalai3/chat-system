package chinggis.systems.service;

import chinggis.systems.model.ChatEntry;
import chinggis.systems.model.Response;

public interface MainService {
	/**
	 * Үндсэн сервер уруу нэвтрэх хүсэлт илгээх
	 * 
	 * @param userName
	 *            хэрэглэгчийн нэвтрэх нэр
	 * @param password
	 *            нэвтрэх нууц үг
	 * @return серверээс ирэх хариу
	 */
	Response loginRequest(String userName, String password);

	/**
	 * Үндсэн сервер уруу бүртгүүлэх хүсэлт илгээх
	 * 
	 * @param userName
	 *            хэрэглэгчийн нэр
	 */
	Response registerRequest(String userName, String password);

	/**
	 * Чат текст илгээх
	 * 
	 * @param chatEntry
	 *            чат өгөгдөл агууламж
	 */
	void sendChatEntry(ChatEntry chatEntry);

	Response readStream();

	/**
	 * Үндсэн сервертэй холбогдох
	 * 
	 * @param serverAddress
	 *            Серверийн хаяг
	 * @param port
	 *            Серверийн портын дугаар
	 */
	void init(String serverAddress, int port);

	/***/
	void close();
}