package chinggis.systems.service;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LogFile {
	
	Logger logger;
    FileHandler fileHandler;
    
    public LogFile() {
    }
    
    public void init() {
    	logger = Logger.getLogger("MyLog");  
    }
    
    public void writeLog(String data) {
    	try {
    		
    		fileHandler = new FileHandler("ChatLog.log");  
    		logger.addHandler(fileHandler);
    		SimpleFormatter formatter = new SimpleFormatter();  
    		fileHandler.setFormatter(formatter);  
    		logger.info("data");  

    	} catch (SecurityException e) {  
    		e.printStackTrace();  
    	} catch (IOException e) {  
    		e.printStackTrace();  
    	} 
    	finally {
    		fileHandler.close();
    	}
    }
}
