package chinggis.systems.view;

import chinggis.systems.model.Response;

public interface ChatView {

	/**
	 * Чат цонх үүсгэх
	 */
	void buildView();

	/**
	 * 
	 * */
	void loadChatEntries(Response response);

	/**
	 * 
	 * */
	void loadUsers(Response response);
	void loadUser(Response response);
	
	void loadHistory(Response response);
}
