package chinggis.systems.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import chinggis.systems.controller.ClientController;
import chinggis.systems.view.ui.LoginWindow;

public class LoginViewImpl implements LoginView {

	private ClientController controller;
	private LoginWindow loginWindow;
	private String errorMessage = "";

	public LoginViewImpl() {
	}

	public LoginViewImpl(ClientController controller) {
		this.controller = controller;
		this.loginWindow = new LoginWindow();
	}

	public void setErrorMessage(String error) {
		errorMessage = error;
	}

	public void buildView() {
		this.loginWindow.buildView();
		addListeners();
	}
	
	@Override
	public void closeView() {
		loginWindow.close();
	}

	private void addListeners() {
		ActionListener listenerLogin = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String name = loginWindow.getUserName();
				String pass = loginWindow.getPasswordValue();
				if (validation(name, pass)) {
					controller.login(name, pass);
				} else {
					sendErrorMessage(name, pass);
					showErrorMsg(errorMessage);
				}
				reset();
			}
		};
		ActionListener listenerRegister = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.buildRegisterView();
				loginWindow.close();
			}
		};
		loginWindow.addLoginButtonClickListener(listenerLogin);
		loginWindow.addRegisterButtonClickListener(listenerRegister);
		ActionListener listenerField = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String name = loginWindow.getUserName();
				String pass = loginWindow.getPasswordValue();
				if (validation(name, pass)) {
					controller.login(name, pass);
				} else {
					sendErrorMessage(name, pass);
					showErrorMsg(errorMessage);
				}
			}
		};
		loginWindow.addLoginButtonClickListener(listenerLogin);
		loginWindow.addRegisterButtonClickListener(listenerRegister);
		loginWindow.addTextFieldKeyListener(listenerField);
	}

	private void sendErrorMessage(String name, String pass) {
		if (name.trim().equals("")) {
			errorMessage += "Та нэрээ оруулна уу! ";
		}
		if (pass.trim().equals("")) {
			errorMessage += "Та нууц үгээ оруулна уу!";
		}
	}

	private boolean validation(String name, String pass) {
		if (name.equals("") || pass.equals("")) {
			return false;
		}
		return true;
	}

	@Override
	public void showErrorMsg(String errorMessage) {
		loginWindow.setErrorMessage(errorMessage);
	}
	
	private void reset() {
		loginWindow.setUserName("");
		loginWindow.setPassword("");
	}
}
