package chinggis.systems.view;

public interface LoginView {
	/**
	 * Нэвтрэх цонхонд алдааны мессеж харуулах
	 * 
	 * @param errorMessage
	 *            Алдааны мэдээлэл
	 */
	void showErrorMsg(String errorMessage);

	/**
	 * Нэвтрэх цонх үүсгэх
	 */
	void buildView();
	
	void closeView();
}
