package chinggis.systems.view.ui;

import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

public class RegisterWindow extends JFrame{
	public RegisterWindow() {
	}
	
	private JTextField textfield_username;
	private JPasswordField passwordField_password;
	private JPasswordField passwordField_verificationPassword;
	private JButton button_register;
	private JLabel errorMessage;
	
	public String getUserName() {
		return textfield_username.getText().toString();
	}
	
	public void setUserName(String userName) {
		textfield_username.setText(userName);
	}
	
	public String getPassword() {
		return String.valueOf(passwordField_password.getPassword());
	}
	
	public void setPassword(String password) {
		passwordField_password.setText(password);
	}
	
	public String getVerificationPassword() {
		return String.valueOf(passwordField_verificationPassword.getPassword());
	}
	
	public void setVerificationPassword(String verificationPassword) {
		passwordField_verificationPassword.setText(verificationPassword);
	}
	
	public void setErrorMessage(String error) {
		errorMessage.setText(error);
	}

	public void buildView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 430, 264);
		setTitle("Бүртгүүлэх");
		SpringLayout springLayout = new SpringLayout();
		getContentPane().setLayout(springLayout);
		
		getContentPane().setBackground(new Color(255, 240, 245));
		getContentPane().setForeground(new Color(255, 228, 225));
		
		JLabel label_username = new JLabel("Нэвтрэх нэр");
		springLayout.putConstraint(SpringLayout.NORTH, label_username, 54, SpringLayout.NORTH, getContentPane());
		getContentPane().add(label_username);
		
		textfield_username = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textfield_username, 51, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, textfield_username, 29, SpringLayout.EAST, label_username);
		springLayout.putConstraint(SpringLayout.EAST, textfield_username, -84, SpringLayout.EAST, getContentPane());
		getContentPane().add(textfield_username);
		textfield_username.setColumns(10);
		
		JLabel label_password = new JLabel("Нууц Үг");
		springLayout.putConstraint(SpringLayout.NORTH, label_password, 22, SpringLayout.SOUTH, label_username);
		springLayout.putConstraint(SpringLayout.WEST, label_password, 61, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, label_username, 0, SpringLayout.WEST, label_password);
		getContentPane().add(label_password);
		
		JLabel label_verificationPassword = new JLabel("Нууц үгээ давт");
		springLayout.putConstraint(SpringLayout.NORTH, label_verificationPassword, 23, SpringLayout.SOUTH, label_password);
		label_verificationPassword.setForeground(new Color(0, 0, 0));
		springLayout.putConstraint(SpringLayout.WEST, label_verificationPassword, 61, SpringLayout.WEST, getContentPane());
		getContentPane().add(label_verificationPassword);
		
		button_register = new JButton("Бүртгүүлэх");
		springLayout.putConstraint(SpringLayout.EAST, button_register, 0, SpringLayout.EAST, textfield_username);
		button_register.setForeground(new Color(0, 0, 0));
		getContentPane().add(button_register);
		
		errorMessage = new JLabel("");
		errorMessage.setForeground(new Color(255, 0, 0));
		springLayout.putConstraint(SpringLayout.WEST, errorMessage, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, errorMessage, -10, SpringLayout.SOUTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, errorMessage, 404, SpringLayout.WEST, getContentPane());
		getContentPane().add(errorMessage);
		
		passwordField_password = new JPasswordField();
		springLayout.putConstraint(SpringLayout.NORTH, passwordField_password, -3, SpringLayout.NORTH, label_password);
		springLayout.putConstraint(SpringLayout.WEST, passwordField_password, 0, SpringLayout.WEST, textfield_username);
		springLayout.putConstraint(SpringLayout.EAST, passwordField_password, 0, SpringLayout.EAST, textfield_username);
		getContentPane().add(passwordField_password);
		
		passwordField_verificationPassword = new JPasswordField();
		springLayout.putConstraint(SpringLayout.NORTH, button_register, 25, SpringLayout.SOUTH, passwordField_verificationPassword);
		springLayout.putConstraint(SpringLayout.NORTH, passwordField_verificationPassword, 14, SpringLayout.SOUTH, passwordField_password);
		springLayout.putConstraint(SpringLayout.WEST, passwordField_verificationPassword, 14, SpringLayout.EAST, label_verificationPassword);
		springLayout.putConstraint(SpringLayout.EAST, passwordField_verificationPassword, 0, SpringLayout.EAST, textfield_username);
		getContentPane().add(passwordField_verificationPassword);
	
		setVisible(true);
	}
	
	public void addRegisterButtonClickListener(ActionListener listenerRegister) {
		button_register.addActionListener(listenerRegister);
	}
	
	public void closeWindow() {
		dispose();
	}
}
