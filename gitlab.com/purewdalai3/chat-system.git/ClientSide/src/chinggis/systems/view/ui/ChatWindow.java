package chinggis.systems.view.ui;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;

public class ChatWindow extends JFrame {
	public ChatWindow() {
	}

	private JPanel contentPane;
	private JTextField tf_content;
	private JList list_chatEntries;
	private JList list_users;
	private JButton btn_send;
	private DefaultListModel model_user;
	private DefaultListModel model_chatEntry;
	private String title;

	public String getContent() {
		return tf_content.getText().toString();
	}

	public void setContent(String content) {
		tf_content.setText(content);
	}

	public void addUsername(String username) {
		model_user.addElement(username);
	}

	public void addChatEntry(String content, String date, String userName) {
		String data = userName + " : " + content;
		model_chatEntry.addElement(data);
		model_chatEntry.addElement(date);
	}

	public void addActionListenerButtonSend(ActionListener actionListener) {
		btn_send.addActionListener(actionListener);
	}

	public void addTextFieldListener(ActionListener fieldListener) {
		tf_content.addActionListener(fieldListener);
	}

	public void addCloseListener(WindowAdapter windowAdapter) {
		this.addWindowListener(windowAdapter);
	}

	public void buildView() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 489, 343);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);

		model_chatEntry = new DefaultListModel();
		list_chatEntries = new JList(model_chatEntry);
		list_chatEntries.setSize(500, 100);
		sl_contentPane.putConstraint(SpringLayout.NORTH, list_chatEntries, 10, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, list_chatEntries, 20, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, list_chatEntries, -49, SpringLayout.SOUTH, contentPane);

		JScrollPane scrollPane_chatEntry_list = new JScrollPane(list_chatEntries);
		sl_contentPane.putConstraint(SpringLayout.NORTH, scrollPane_chatEntry_list, 10, SpringLayout.NORTH,
				contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, scrollPane_chatEntry_list, 20, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, scrollPane_chatEntry_list, -49, SpringLayout.SOUTH,
				contentPane);
		scrollPane_chatEntry_list.setSize(500, 100);
		contentPane.add(scrollPane_chatEntry_list);

		model_user = new DefaultListModel();
		list_users = new JList(model_user);
		list_users.setSize(new Dimension(100, 100));
		sl_contentPane.putConstraint(SpringLayout.WEST, list_users, 307, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, list_users, -15, SpringLayout.EAST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, list_chatEntries, -17, SpringLayout.WEST, list_users);
		sl_contentPane.putConstraint(SpringLayout.NORTH, list_users, 5, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, list_users, 284, SpringLayout.NORTH, contentPane);

		JScrollPane scrollPane_user_list = new JScrollPane(list_users);
		sl_contentPane.putConstraint(SpringLayout.WEST, scrollPane_user_list, 307, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, scrollPane_user_list, -15, SpringLayout.EAST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, list_chatEntries, -17, SpringLayout.WEST, scrollPane_user_list);
		sl_contentPane.putConstraint(SpringLayout.NORTH, scrollPane_user_list, 5, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, scrollPane_user_list, 284, SpringLayout.NORTH, contentPane);

		contentPane.add(scrollPane_user_list);

		btn_send = new JButton("Илгээх");
		sl_contentPane.putConstraint(SpringLayout.SOUTH, btn_send, -10, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, btn_send, -5, SpringLayout.EAST, list_chatEntries);
		contentPane.add(btn_send);

		tf_content = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, btn_send, 0, SpringLayout.NORTH, tf_content);
		sl_contentPane.putConstraint(SpringLayout.WEST, btn_send, 6, SpringLayout.EAST, tf_content);
		sl_contentPane.putConstraint(SpringLayout.WEST, tf_content, 20, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, tf_content, -283, SpringLayout.EAST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.NORTH, tf_content, -40, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, tf_content, 0, SpringLayout.SOUTH, list_users);
		contentPane.add(tf_content);
		tf_content.setColumns(10);
		setVisible(true);
	}
}
