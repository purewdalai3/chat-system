package chinggis.systems.view;

public interface RegisterView {
	/**
	 * Бүртгүүлэх цонхонд алдааны мессеж харуулах
	 * 
	 * @param errorMessage
	 *            Алдааны мэдээлэл
	 */
	void showErrorMessage(String errorMessage);

	/*
	 * Бүртгүүлэх цонх үүсгэх
	 */
	void buildView();

	void closeView();
}
