package chinggis.systems.view.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import chinggis.systems.controller.ClientController;

public class LoginWindow extends JFrame {
	
	JTextField tf_username;
	private JButton button_login;
	JLabel lblError;
	private JButton button_register;
	JPasswordField pf_password;

	public void setErrorMessage(String error) {
		lblError.setText(error);
	}
	
	public String getUserName()
	{
		return tf_username.getText().trim().toLowerCase().toString();
	}
	
	public void setUserName(String userName) {
		tf_username.setText(userName);
	}
	
	public String getPasswordValue()
	{
		return String.valueOf(pf_password.getPassword());
	}
	
	public void setPassword(String password) {
		pf_password.setText(password);
	}
	
	public void buildView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 430, 264);
		setTitle("Нэвтрэх цонх");
		SpringLayout springLayout = new SpringLayout();
		getContentPane().setLayout(springLayout);

		JLabel label = new JLabel("Таны нэр :");
		springLayout.putConstraint(SpringLayout.NORTH, label, 60, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, label, -293, SpringLayout.EAST, getContentPane());
		getContentPane().add(label);

		tf_username = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_username, 57, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, tf_username, 39, SpringLayout.EAST, label);
		springLayout.putConstraint(SpringLayout.EAST, tf_username, -74, SpringLayout.EAST, getContentPane());
		getContentPane().add(tf_username);
		tf_username.setColumns(10);

		JLabel label_1 = new JLabel("Таны нууц үг :");
		springLayout.putConstraint(SpringLayout.NORTH, label_1, 33, SpringLayout.SOUTH, label);
		springLayout.putConstraint(SpringLayout.EAST, label_1, -293, SpringLayout.EAST, getContentPane());
		getContentPane().add(label_1);

		button_login = new JButton("Нэвтрэх");
		springLayout.putConstraint(SpringLayout.NORTH, button_login, 64, SpringLayout.SOUTH, tf_username);
		springLayout.putConstraint(SpringLayout.EAST, button_login, 0, SpringLayout.EAST, tf_username);
		getContentPane().add(button_login);

		lblError = new JLabel("");
		springLayout.putConstraint(SpringLayout.WEST, lblError, 0, SpringLayout.WEST, label_1);
		springLayout.putConstraint(SpringLayout.SOUTH, lblError, -31, SpringLayout.SOUTH, getContentPane());
		getContentPane().add(lblError);

		button_register = new JButton("Бүртгүүлэх");
		springLayout.putConstraint(SpringLayout.SOUTH, button_register, 0, SpringLayout.SOUTH, button_login);
		springLayout.putConstraint(SpringLayout.EAST, button_register, -23, SpringLayout.WEST, button_login);
		getContentPane().add(button_register);
		
		pf_password = new JPasswordField();
		springLayout.putConstraint(SpringLayout.WEST, pf_password, 0, SpringLayout.WEST, tf_username);
		springLayout.putConstraint(SpringLayout.SOUTH, pf_password, -17, SpringLayout.NORTH, button_login);
		springLayout.putConstraint(SpringLayout.EAST, pf_password, 0, SpringLayout.EAST, tf_username);
		getContentPane().add(pf_password);

		// button_register.addActionListener(this);

		setVisible(true);

		// button_login.addActionListener(this);
	}

	public void addLoginButtonClickListener(ActionListener listenerLogin) {
		button_login.addActionListener(listenerLogin);
	}
	
	public void addRegisterButtonClickListener(ActionListener listenerRegister) {
		button_register.addActionListener(listenerRegister);
	}
	public void addTextFieldKeyListener(ActionListener listenerField)
	{
		pf_password.addActionListener(listenerField);
		tf_username.addActionListener(listenerField);
	}
	
	public void close()
	{
		this.dispose();
	}
}
