package chinggis.systems.model;

public class User {
	String userName;
	String password;
	int chatId;

	public User() {
	}

	public User(String userName, String password, int chatId) {
		this.userName = userName;
		this.password = password;
		this.chatId = chatId;
	}

	public int getChatId() {
		return chatId;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setChatId(int chatId) {
		this.chatId = chatId;
	}
}
