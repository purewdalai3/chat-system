package chinggis.systems.model;

public class Response {
	String type; 
	String json;
	
	public Response() {}
	
	public Response(String type , String json) {
		this.type = type;
		this.json = json;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public void setJson(String json) {
		this.json = json;
	}
	
	public String getType() {
		return type;
	}
	
	public String getJson() {
		return json;
	}
}
